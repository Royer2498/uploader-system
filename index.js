const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const cors = require('cors');
const cron = require('node-cron');
const port = 3000;
const fs = require('fs');
const firebase = require("firebase/app");
const { Storage } = require('@google-cloud/storage');
const { trepFilesNames } = require('./assets/file-paths/trep');
const { computoFilesNames } = require('./assets/file-paths/computo');
const axios = require('axios').default;

require("firebase/auth");
require("firebase/firestore");
require("firebase/storage")

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const firebaseConfig = {
    apiKey: "AIzaSyDU5saZfSyQrwXmm25o9SjXkjzbUq_KZAc",
    authDomain: "fake-direpre-1602183483241.firebaseapp.com",
    databaseURL: "https://fake-direpre-1602183483241.firebaseio.com",
    projectId: "fake-direpre-1602183483241",
    storageBucket: "fake-direpre-1602183483241.appspot.com",
    messagingSenderId: "238436802212",
    appId: "1:238436802212:web:91181946998dceccb7d031"
};

firebase.initializeApp(firebaseConfig);

const storage = new Storage({
    projectId: "fake-direpre-1602183483241",
    keyFilename: "./fake-direpre-1602183483241-firebase-adminsdk-eyc35-ba653d7835.json"
});

var trepFiles = trepFilesNames
var computeFiles = computoFilesNames

var trepCounter = 0;
var computeCounter = 0;

var trepResponseFileName = trepFiles[0];
var computeResponseFileName = computeFiles[0];

const remoteUrl = 'https://computo.oep.org.bo/api/v1/resultado/mesa';


cron.schedule('*/2 * * * *', () => {
    trepResponseFileName = trepFiles[trepCounter++];
    computeResponseFileName = computeFiles[computeCounter++];
    if (trepCounter == trepFiles.length) {
        trepCounter = 0;
    }
    if (computeCounter == computeFiles.length) {
        trepCounter = 0;
    }
});


app.get('/results/trep', function(req, res) {
    res.download(`${__dirname}/assets/excels/trep/acta.2019.10.21.22.23.53.xlsx`);
});

app.get('/results/trep2', function(req, res) {
    var filePath = `trep/${trepResponseFileName}`;
    var destinationPath = `${__dirname}/assets/excels/trep/${trepResponseFileName}`
    storage.bucket('fake-direpre-1602183483241.appspot.com')
        .file(filePath)
        .download({destination: destinationPath})
        .then(() => {
            console.log("Success");
            res.download(destinationPath, () => {
                fs.unlinkSync(destinationPath);
            }).catch((error) => {
                 console.log("Error en el res.download papa")
                console.log(error)
            });
    }).catch((error) => {
        console.log(error)
    });
    //res.download(`${__dirname}/assets/excels/trep/${trepResponseFileName}`);
});

app.get('/results/computo', function(req, res) {
    const computeResponseFileName = 'SCORC.xlsx'
    res.download(`${__dirname}/assets/excels/computo/${computeResponseFileName}`);
 });

 app.get('/results/computo/image', function(req, res) {
    var image = 'imagen.jpeg';
    res.download(`${__dirname}/assets/images/${image}`);
 });

 app.get('/upload-results', async (req, res) => {
    let tableCodes = req.body.tableCodes;
    let calls = [];
    tableCodes.forEach((code) => {
        calls.push(getTableInformation(code))
    });
    Promise.all(calls).then((response) => {
        console.log(response);
        // Parseaer la info a lo que necesita armando 
        // Post a los 3 endpoints de armando
        res.send({ ok: true})
    });
});

let downloadImage = (url) => {
    //Descargar imagen localmente 
}

let getTableInformation = async (tableCode) => {
    let information = {
        data: [],
        imageUrl: ''
    }    
    try {
        let response = await axios.post(remoteUrl, { codigoMesa: tableCode });
        information.data = response.data.datoAdicional.tabla;
        information.imageUrl = response.data.datoAdicional.adjunto[0].valor;
        return information;
    } catch(error) {
        console.log(error);
    }
};

app.listen(process.env.PORT || port, () => {
    console.log('Escuchando en el puerto ' + port)
});